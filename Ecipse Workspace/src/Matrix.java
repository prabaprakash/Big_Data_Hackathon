import java.io.IOException;
import java.util.*;
import java.util.AbstractMap.SimpleEntry;
import java.util.Map.Entry;



import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;





public class Matrix {

	public static class TokenizerMapper extends
			Mapper<Text, Text, Text, Text> 
	{

		
		

		public void map(Text key, Text value, Context context)
				throws IOException, InterruptedException
				{
			    String[] line=value.toString().split(",");
			    Text k=new Text();
			    Text v=new Text();
			    if(line[0]=="A"){
			    	k.set(line[2]);;
			    	v.set("A,"+line[1]+","+line[3]);
			    	//System.out.println(v);
			    	context.write(k, v);	    	
			    }else {
			    	k.set(line[1]);;
			    	v.set("B,"+line[2]+","+line[3]);
			    	context.write(k, v);
				}
	
				
			
			}
	}
  
	public static class IntSumReducer extends
			Reducer<Text, Text, Text, Text> {
		private IntWritable result = new IntWritable();

		public void reduce(Text key, Iterable<Text> values,
				Context context) throws IOException, InterruptedException {
			String[] list;
			int i;
			float val;
			 
			ArrayList<Entry<Integer,Float>> lista=new ArrayList<Entry<Integer,Float>>();
			ArrayList<Entry<Integer,Float>> listb=new ArrayList<Entry<Integer,Float>>();
			
			for (Text t : values) {
				list=t.toString().split(",");
				if(list[0]=="A")
				{  
				 i=Integer.parseInt(list[1]);
				 val=(float)Integer.parseInt(list[2]);
				 lista.add(new SimpleEntry<Integer,Float>(i,val));
				}
				else {
					 i=Integer.parseInt(list[1]);
					 val=(float)Integer.parseInt(list[2]);
					 listb.add(new SimpleEntry<Integer,Float>(i,val));
				}
				for (Entry<Integer, Float> enta : lista) {
					
					
					for (Entry<Integer, Float> entb : lista) {
						float c=enta.getValue()*entb.getValue();
						context.write(null, new Text(Float.toString(c)));
					}
				}
				
			}
			context.write(key, new Text());
		
		}
	}

	public static void main(String[] args) throws Exception {
		Configuration conf = new Configuration();
		//conf.addResource(new Path("C:/hadoop-2.3.0/etc/hadoop/core-site.xml"));
	    //conf.addResource(new Path("C:/hadoop-2.3.0/etc/hadoop/hdfs-site.xml"));
		
		@SuppressWarnings("deprecation")
		Job job = new Job(conf, "Matrix");
		job.setJarByClass(Matrix.class);
		job.setMapperClass(TokenizerMapper.class);
		job.setCombinerClass(IntSumReducer.class);
		job.setReducerClass(IntSumReducer.class);
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(Text.class);
		//FileInputFormat.setInputPaths(job, new Path("/input"));
		//FileOutputFormat.setOutputPath(job, new Path("/output"));
		    FileInputFormat.addInputPath(job, new Path("hdfs://127.0.0.1:9000/matrixin"));
		    FileOutputFormat.setOutputPath(job, new Path("hdfs://127.0.0.1:9000/matrixout"));
	//	job.mapProgress();
	 job.submit();
	 job.mapProgress();
	 job.reduceProgress();
		//System.exit(job.waitForCompletion(true) ? 0 : 1);
	 //You can run a MapReduce job with a single line of code: JobClient.runJob(conf).
	 // It�s very short, but it conceals a great deal of processing behind the scenes. //
	 //This section uncovers the steps Hadoop takes to run a job.
	}
}